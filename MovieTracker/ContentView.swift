//
//  ContentView.swift
//  MovieTracker
//
//  Created by วิโรจน์ ขุนทอง on 16/8/2564 BE.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!").background(Color.blue)
            .padding().background(Color.green)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
